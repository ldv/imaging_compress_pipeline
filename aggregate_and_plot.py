#!/usr/bin/env python3
import argparse
import subprocess
import json
from lofar_quality.manipulate.common import join_inspect_ds
from lofar_quality.inspect.inspect_dataset import plot_inspect_ds
from lofar_quality.inspect.v2 import extract_quality_metrics
from lofar_quality.inspect.common import store_metrics
from pathlib import Path
import logging

FORMAT = '%(levelname)-15s %(asctime)s: %(message)s'
logging.basicConfig(format=FORMAT, level="INFO")

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Process H5 files and output results to a specified directory.")
    parser.add_argument('workdir', type=str, help='Path to the working directory')

    # Parse the arguments
    args = parser.parse_args()
    workdir = Path(args.workdir)  # The first command-line argument

    h5_paths = []
    with open(workdir / "inputs.txt", "r") as file:
        for line in file:
            line = str(line.strip())  # Remove white space
            path = Path(line)
            if path.exists():
                if path.suffix.lower() == ".h5":
                    h5_paths.append(line)
                else:
                    logging.info(f"File exists but is not an h5 file: {line}")
            else:
                logging.info(f"Could not find the following input file: {line}")
        logging.info(f"Found {len(h5_paths)} h5 files")

    h5_output_path = "inspect.h5"
    join_inspect_ds(h5_paths, h5_output_path)
    plot_paths = plot_inspect_ds(h5_output_path, workdir, min_elevation=15)

    quality = {"summary": extract_quality_metrics(h5_output_path)}
    store_metrics("outputs.json", quality)

    with open("outputs.txt", "w") as file:
        file.write(str(h5_output_path) + "\n")
        for file_path in plot_paths:
            path = Path(file_path)
            file.write(str(path) + "\n")

if __name__ == "__main__":
    main()

