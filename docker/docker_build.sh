#!/bin/bash

# Build the docker image, using the git commit hashes for
# the packages we need to build.

# Ensure a TAG is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <image_name> <tag>"
  exit 1
fi

IMAGE_NAME=$1
TAG=$2

docker build \
  --build-arg CASACORE_COMMIT=master \
  --build-arg LOFARSTMAN_COMMIT=main \
  --build-arg AOFLAGGER_COMMIT=master \
  --build-arg EVERYBEAM_COMMIT=master \
  --build-arg DP3_COMMIT=master \
  --build-arg WSCLEAN_COMMIT=master \
  --build-arg PYTHONCASACORE_COMMIT=master \
  --progress plain \
  --file docker/Dockerfile \
  --tag ${IMAGE_NAME}:${TAG} \
  --tag ${IMAGE_NAME}:latest \
  .

  echo "Succesfully build docker file"
