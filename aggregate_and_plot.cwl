cwlVersion: v1.0
class: CommandLineTool

requirements:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.5.1

baseCommand: python3
arguments:
  - $(inputs.workdir)/imaging_compress_pipeline.git/aggregate_and_plot.py

inputs:
  workdir:
    type: string
    inputBinding:
      position: 1  # The first argument after the script will be the workdir

outputs:
- id: plots
  type: File[]
  outputBinding:
    glob: "*.png"
- id: outputs
  type: File
  outputBinding:
    glob: "outputs.txt"
- id: quality
  type: File
  outputBinding:
    glob: "outputs.json"
- id: inspect
  type: File
  outputBinding:
    glob: "inspect.h5"
