#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

inputs:
- id: ms
  type: Directory[]

outputs:
- id: inspect
  type: File
  outputSource:
    combine/inspect_file
- id: quality
  type: Any
  outputSource:
    format_metrics/formatted_quality

requirements:
- class: ScatterFeatureRequirement
steps:
- id: create
  in: 
  - id: msin
    source: ms
  out:
  - id: inspect_file
  scatter: msin
  run: ../steps/create_inspect_dataset.cwl
- id: combine
  in:
  - id: inputs
    source: create/inspect_file
  out:
  - id: inspect_file
  run: ../steps/combine_inspect_dataset.cwl
- id: extract
  in: 
  - id: inspect
    source: combine/inspect_file
  out:
   - id: quality
   - id: plots
  run: ../steps/extract_quality_metrics.cwl
- id: format_metrics
  in:
  - id: quality
    source: extract/quality
  - id: plots
    source: extract/plots
  out:
  - formatted_quality
  run: ../steps/format_metrics.cwl