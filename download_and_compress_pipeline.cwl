#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow
label: download_and_compress_pipeline

requirements:
- class: ScatterFeatureRequirement
- class: SubworkflowFeatureRequirement
- class: StepInputExpressionRequirement
- class: InlineJavascriptRequirement

inputs:
- id: flag_autocorrelation
  type: boolean?
- id: surls
  type: string[]
- id: is_long_baseline
  type: boolean?

outputs:
- id: compressed
  type: File[]
  outputSource:
  - compress/compressed
- id: logfile
  type:
    type: array
    items:
      type: array
      items: File
  outputSource:
  - compress/logfile
- id: ingest
  type: Any[]
  outputSource:
  - compress/ingest
- id: quality
  type: Any
  outputSource:
  - format_quality_metrics/formatted_quality
- id: control
  type: Any
  outputSource:
  - format_quality_metrics/control
- id: inspect
  type: File
  outputSource:
  - combine/inspect_file

steps:
- id: fetch_data
  label: fetch_data
  in:
  - id: surl_link
    source: surls
  scatter:
  - surl_link
  run: steps/fetch_data.cwl
  out:
  - id: uncompressed
- id: compress
  in:
  - id: msin
    source: fetch_data/uncompressed
  - id: flag_autocorrelation
    source: flag_autocorrelation
  - id: is_long_baseline
    source: is_long_baseline
  scatter:
  - msin
  run: ./compress_pipeline.cwl
  out:
  - id: compressed
  - id: inspect
  - id: logfile
  - id: ingest
  - id: keep_input
  - id: applied_fixes
  - id: unfixable_issues
  - id: input_storage_manager
- id: combine
  in:
  - id: inputs
    source: compress/inspect
  run: steps/combine_inspect_dataset.cwl
  out:
  - inspect_file
- id: extract_metrics
  in:
  - id: inspect
    source: combine/inspect_file
  run: steps/extract_quality_metrics.cwl
  out:
  - id: plots
  - id: quality
- id: format_quality_metrics
  in:
  - id: plots
    source: extract_metrics/plots
  - id: quality
    source: extract_metrics/quality
  - id: msin
    source: fetch_data/uncompressed
  - id: keep_input
    source: compress/keep_input
  - id: applied_fixes
    source: compress/applied_fixes
  - id: unfixable_issues
    source: compress/unfixable_issues
  - id: input_storage_manager
    source: compress/input_storage_manager
  run: steps/format_metrics.cwl
  out:
  - formatted_quality
  - control
