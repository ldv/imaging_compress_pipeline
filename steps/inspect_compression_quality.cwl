class: CommandLineTool
cwlVersion: v1.1
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: inspect_compression_quality
baseCommand: []
inputs:
  - id: before_compression
    type: Directory
    inputBinding:
      shellQuote: false
      position: 0
  - id: after_compression
    type: Directory
    inputBinding:
      shellQuote: false
      position: 0
outputs:
  - id: output
    type: 'File[]'
label: inspect_compression_quality
requirements:
  - class: ShellCommandRequirement
