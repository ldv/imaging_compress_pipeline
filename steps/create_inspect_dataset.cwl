cwlVersion: 'v1.2'
class: CommandLineTool
baseCommand: 
  - inspect_utils.py
  - create

requirements:
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
  - class: NetworkAccess
    networkAccess: true
inputs: 
- id: msin
  type: Directory
  inputBinding: 
    position: 1
- id: inspect_file_name
  type: string?
  default: 'inspect.h5'
  inputBinding:
    position: 2
outputs: 
- id: inspect_file
  type: File
  outputBinding: 
    glob: $(inputs.inspect_file_name)