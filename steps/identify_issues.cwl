#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool
label: Identify Known issues

requirements:
- class: InlineJavascriptRequirement
- class: InitialWorkDirRequirement
  listing:
  - entryname: $(inputs.msin.basename)
    writable: true
    entry: $(inputs.msin)

inputs:
- id: msin
  type: Directory
  inputBinding:
    position: 1
- id: is_long_baseline
  type: boolean?
  inputBinding:
    prefix: --is_long_baseline
    position: 2

outputs:
- id: issue_list
  type: string[]
  outputBinding:
    glob: output.txt
    outputEval: $(JSON.parse(self[0].contents)['fixable_issues'])
    loadContents: true
- id: unfixable_issues
  type: string[]
  outputBinding:
    glob: output.txt
    outputEval: $(JSON.parse(self[0].contents)['unfixable_issues'])
    loadContents: true
- id: is_raw
  type: boolean
  outputBinding:
    glob: output.txt
    outputEval: $(JSON.parse(self[0].contents)['is_raw'])
    loadContents: true
- id: skip_compress
  type: boolean
  outputBinding:
    glob: output.txt
    outputEval: $(JSON.parse(self[0].contents)['skip_compress'])
    loadContents: true
- id: input_storage_manager
  type: string
  outputBinding:
    glob: output.txt
    outputEval: $(JSON.parse(self[0].contents)['input_storage_manager'])
    loadContents: true
- id: msout
  type: Directory
  outputBinding:
    glob: $(inputs.msin.basename)
stdout: output.txt

baseCommand:
- fix_common_ms_issues.py

hints:
- class: DockerRequirement
  dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
- class: NetworkAccess
  networkAccess: true
id: identify_issues
