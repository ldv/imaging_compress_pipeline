#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: ExpressionTool

requirements:
- class: InlineJavascriptRequirement

inputs:
- id: plots
  type: File[]
- id: quality
  type: Any
- id: msin
  type: Directory[]
- id: keep_input
  type: boolean[]
- id: applied_fixes
  type:
    type: array
    items:
      type: array
      items: string
- id: unfixable_issues
  type:
    type: array
    items:
      type: array
      items: string
- id: input_storage_manager
  type:
    type: array
    items: string

outputs:
- id: formatted_quality
  type: Any
- id: control
  type: Any

expression: |
  ${

    var quality = {
      'plots': inputs.plots,
      'summary': inputs.quality
    };

    quality['summary']['applied_fixes'] = inputs.applied_fixes[0];
    quality['summary']['unfixable_issues'] = inputs.unfixable_issues[0];
    quality['summary']['input_storage_manager'] = inputs.input_storage_manager;
    quality['summary']['already_compressed'] = inputs.input_storage_manager.map(value => value === "DyscoStorageManager");

    var control = {
      'keep_input': inputs.msin.map((file, index) => {
        if (inputs.keep_input[index]) {
          return {
            surl: file.location
          };
        }
        return null;
      }).filter(item => item !== null)
    };

    return {
      'formatted_quality': quality,
      'control': control
    };
  }
