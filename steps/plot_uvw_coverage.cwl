cwlVersion: v1.1
class: CommandLineTool
baseCommand:
  - python3
  - script.py
inputs:
  - id: input_file
    type: File
    inputBinding:
      position: 0
  - id: output_name
    type: string?
    default: 'uv_coverage.png'
    inputBinding:
      position: 1
outputs:
  - id: uv_plot
    type: File
    outputBinding:
      glob: $(inputs.output_name)
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
  - class: NetworkAccess
    networkAccess: true

requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entryname: script.py
        entry: |
          import h5py
          import matplotlib.pylab as plt
          import sys
          import numpy as np
          from matplotlib import cm

          h5_file = h5py.File(sys.argv[1], 'r')
          output_name = sys.argv[2]
          u = h5_file['URANGE']
          v = h5_file['VRANGE']

          plt.imshow(np.log10(h5_file['COVERAGE'][:, :, 0]), extent= [u[0], u[-1], v[0], v[-1]], cmap=cm.Blues)
          plt.imshow(np.log10(h5_file['FLAGS'][:, :, 0]), extent= [u[0], u[-1], v[0], v[-1]] , cmap=cm.Reds)
          plt.imshow(np.log10(h5_file['DATALOSS'][:, :, 0]), extent= [u[0], u[-1], v[0], v[-1]] , cmap=cm.Greens)

          plt.savefig(output_name)
