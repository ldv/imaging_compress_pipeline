#!/usr/bin/env cwl-runner

cwlVersion: v1.1
class: CommandLineTool

requirements:
- class: InlineJavascriptRequirement

inputs:
- id: parset
  type: File?
  inputBinding:
    position: -1
- id: msin
  doc: Input Measurement Set
  type: Directory?
  inputBinding:
    prefix: msin=
    position: 0
    separate: false
- id: msout_name
  doc: Output Measurement Set
  type: string
  default: .
  inputBinding:
    prefix: msout=
    position: 0
    separate: false
- id: autoweight
  type: boolean
  default: false
  inputBinding:
    prefix: msin.autoweight=True
    position: 0
- id: baseline
  type: string
  default: ''
  inputBinding:
    prefix: msin.baseline=
    position: 0
    separate: false
- id: output_column
  type: string?
  default: DATA
  inputBinding:
    prefix: msout.datacolumn=
    position: 0
    separate: false
- id: input_column
  type: string?
  default: DATA
  inputBinding:
    prefix: msin.datacolumn=
    position: 0
    separate: false
- id: writefullresflag
  type: boolean
  default: false
  inputBinding:
    prefix: msout.writefullresflag=True
- id: overwrite
  type: boolean
  default: false
  inputBinding:
    prefix: msout.overwrite=True
- id: storagemanager
  type: string
  default: ''
  inputBinding:
    prefix: msout.storagemanager=
    separate: false
- id: databitrate
  type: int
  default: 0
  inputBinding:
    prefix: msout.storagemanager.databitrate=
    separate: false

outputs:
- id: msout
  doc: Output Measurement Set
  type: Directory
  outputBinding:
    glob: '$(inputs.msout_name=="." ? inputs.msin.basename : inputs.msout_name)'
- id: logfile
  type: File[]
  outputBinding:
    glob: '*DPPP*.log'
stdout: $(inputs.msin.basename)_DPPP.log
stderr: $(inputs.msin.basename)_DPPP_err.log

baseCommand:
- DP3

hints:
- class: DockerRequirement
  dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
- class: NetworkAccess
  networkAccess: true
id: dppp
