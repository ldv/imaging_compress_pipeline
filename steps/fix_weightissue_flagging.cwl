id: fix_weight_issue_flagging
label: Fix Weight Issue Flagging
cwlVersion: v1.2
class: CommandLineTool
baseCommand:
  - bash
  - script.sh
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
        entryname: $(inputs.msin.basename)
      - entryname: script.sh
        entry: |
          #!/bin/bash

          execute=$(inputs.apply)
          if [ $execute = 'true' ] ; then
            echo "Appling fix weight issue flagging"
            fix_weightspectrum $(inputs.msin.basename)
          fi
          echo "Skipping apply fix weight issue flagging"
  - class: InplaceUpdateRequirement
    inplaceUpdate: true
inputs:
  - id: msin
    type: Directory
  - id: apply
    type: boolean
outputs:
  - id: msout
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
  - class: NetworkAccess
    networkAccess: true