#!/usr/bin/env cwl-runner

cwlVersion: v1.1
class: CommandLineTool
label: define_parset

requirements:
- class: ShellCommandRequirement
- class: InitialWorkDirRequirement
  listing:
  - entryname: input.parset
    entry: |
      steps=[flagedge,flagelev$('is_raw'? ',aoflag,flagamp': '')$(inputs.demix? ',demix': '')]
      #
      flagedge.chan=[0..nchan/32-1,31*nchan/32..nchan-1]
      flagedge.type=preflagger
      #
      aoflag.keepstatistics=true
      aoflag.memoryperc=20
      aoflag.type=aoflagger
      aoflag.autocorr=$(inputs.flag_autocorrelation?'True':'False')
      aoflag.strategy=/usr/local/share/aoflagger/strategies/lofar-default.lua
      #
      flagelev.type=preflagger
      flagelev.elevation=0deg..20deg
      #
      flagamp.amplmin=1e-30
      flagamp.type=preflagger
- class: InlineJavascriptRequirement

inputs:
- id: flag_autocorrelation
  type: boolean?
  default: true
- id: is_raw
  type: boolean
  default: true

outputs:
- id: output
  type: File
  outputBinding:
    glob: preproc.parset

baseCommand:
- cp
arguments:
- prefix: ''
  position: 0
  valueFrom: input.parset
  shellQuote: false
- prefix: ''
  position: 0
  valueFrom: preproc.parset
  shellQuote: false
id: define_parset
