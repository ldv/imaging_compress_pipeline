cwlVersion: v1.2
class: ExpressionTool
inputs:
  - id: metadata
    type: Any

  - id: output_name
    type: string
  - id: file_name
    type: string
outputs:
  - id: ingest
    type: Any
requirements:
  - class: InlineJavascriptRequirement
expression: |
  ${
    return { "ingest": {
        "path": inputs.output_name,
        "file_name": inputs.file_name,
        "metadata": inputs.metadata
      }
    }
  }
