id: fetchdata
label: fetch_data
class: CommandLineTool
cwlVersion: v1.1
inputs:
  - id: surl_link
    type: string
    inputBinding:
      position: 0

outputs:
  - id: uncompressed
    type: Directory
    outputBinding:
      glob: 'out/*'
baseCommand:
 - 'bash'
 - 'fetch.sh'
doc: 'Untar a compressed file'
requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'fetch.sh'
        entry: |
          #!/bin/bash
          mkdir out
          cd out
          turl=`echo $1 | awk '{gsub("srm://srm.grid.sara.nl[:0-9]*","gsiftp://gridftp.grid.sara.nl"); print}'`
          turl=`echo $turl | awk '{gsub("srm://lta-head.lofar.psnc.pl[:0-9]*","gsiftp://gridftp.lofar.psnc.pl"); print}'`
          turl=`echo $turl | awk '{gsub("srm://lofar-srm.fz-juelich.de[:0-9]*","gsiftp://lofar-gridftp.fz-juelich.de"); print}'`
          echo "Downloading $turl"
          globus-url-copy $turl - | tar -xvf -
