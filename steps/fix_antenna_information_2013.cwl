id: fix_antenna_information_2013
label: Fix Antenna Information 2013
cwlVersion: v1.2
class: CommandLineTool
baseCommand:
  - bash
  - script.sh
inputs:
  - id: msin
    type: Directory
  - id: apply
    type: boolean
outputs:
  - id: msout
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)

requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
        entryname: $(inputs.msin.basename)
      - entryname: script.sh
        entry: |
          #!/bin/bash

          execute=$(inputs.apply)
          if [ $execute = 'true' ] ; then
            echo "Appling fix antenna information 2013"
            fixinfo $(inputs.msin.basename) /opt/fixinfo
          fi
          echo "Skipping apply fix antenna information 2013"

  - class: InplaceUpdateRequirement
    inplaceUpdate: true
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
  - class: NetworkAccess
    networkAccess: true