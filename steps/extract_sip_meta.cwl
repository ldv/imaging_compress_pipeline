class: CommandLineTool
cwlVersion: v1.1
id: inspect_flagging_dataloss
baseCommand:
  - bash
  - script.sh
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
  - id: compressed_file
    type: File
    inputBinding:
      position: 1
outputs:
  - id: ingest
    type: Any
    outputBinding:
      glob: metadata.json
      loadContents: True
      outputEval: |
        ${
          return JSON.parse(self[0].contents)
         }
label: inspect_flagging_dataloss
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
  - class: NetworkAccess
    networkAccess: true

requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
     - entryname: pipeline_info.json
       entry: |
        {
            "pipelineRun": {
            "_type": "AveragingPipeline",
            "demixing": "false",
            "pipelineName": "compress_pipeline",
            "strategyName": "Compression pipeline",
            "pipelineVersion": "v0.3.0",
            "strategyDescription": "Compression with system issues fix",
            "timeIntegrationStep": "1",
            "flagAutoCorrelations": "true",
            "frequencyIntegrationStep": "1",
            "numberOfCorrelatedDataProducts": "1"
          }
         }
     - entryname: dataproduct.json
       entry: |
         {
            "dataProduct":
              {
                "fileName": $(inputs.compressed_file.basename),
                "size": $(inputs.compressed_file.size),
                "fileFormat": "AIPS++/CASA",
              }
         }
     - entryname: script.sh
       entry: |
         md5=`md5sum $2 | awk '{ print $1 }'`
         cat > md5_sum.json << EOF
         {
             "dataProduct":
               {
                 "fileName": $(inputs.compressed_file.basename),
                 "size": $(inputs.compressed_file.size),
                 "md5checksum": $md5
               }
         }
         EOF
         lofar_sip_from_ms.py --json metadata.json $1
         lofar_sip_merge.py metadata.json pipeline_info.json --json metadata.json
         lofar_sip_merge.py metadata.json dataproduct.json --json metadata.json
         lofar_sip_merge.py metadata.json md5_sum.json --json metadata.json
