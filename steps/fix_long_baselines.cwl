id: fix_long_baseline
label: Fix weighted sum for long baseline pipeline
cwlVersion: v1.2
class: CommandLineTool
baseCommand:
  - bash
  - script.sh

requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
        entryname: $(inputs.msin.basename)
      - entryname: script.sh
        entry: |
          #!/bin/bash

          execute=$(inputs.apply)
          if [ $execute = 'true' ] ; then
            echo "Appling fix weighted sum UVW"
            fix_weightedsum_uvw -f $(inputs.msin.basename) -n $(inputs.new_station)
          fi
          echo "Skipping apply fix weighted sum UVW"

  - class: InplaceUpdateRequirement
    inplaceUpdate: true
inputs:
  - id: msin
    type: Directory
  - id: new_station
    doc: new station name (default ST001)
    type: string?
    default: ST001
  - id: apply
    type: boolean
outputs:
  - id: msout
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)

hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
  - class: NetworkAccess
    networkAccess: true