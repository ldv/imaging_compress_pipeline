cwlVersion: 'v1.2'
class: CommandLineTool
baseCommand: 
  - bash
  - script.sh

requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entryname: script.sh
        entry: | 
          #!/bin/bash
          inspect_utils.py plot $(inputs.inspect.path) plots
          inspect_utils.py inspect $(inputs.inspect.path) quality.json
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ldv/imaging_compress_pipeline:v0.4.1
  - class: NetworkAccess
    networkAccess: true
inputs: 
- id: inspect
  type: File
outputs: 
- id: plots
  type: File[]
  outputBinding: 
    glob: plots/*.png
- id: quality
  type: Any
  outputBinding:
    glob: quality.json
    loadContents: true
    outputEval: $(JSON.parse(self[0].contents))