#!/usr/bin/sh
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
TOIL_VENV_DIR="${SCRIPT_DIR}/toil_venv"
if [ -d "toil_venv" ]; then
    echo "Activating existing virtual environment."
    source "${TOIL_VENV_DIR}/bin/activate"
else
    echo "Creating new virtual environment for using toil"
    python3.9 -m venv $TOIL_VENV_DIR
    source "${TOIL_VENV_DIR}/bin/activate"
    pip install --upgrade pip setuptools wheel
    pip install toil[cwl] singularity
fi
export SINGULARITY_BIND="/project:/project"
export TOIL_SLURM_ARGS=""
toil-cwl-runner "${SCRIPT_DIR}/imaging_compress_pipeline.git/aggregate_and_plot.cwl" --workdir $SCRIPT_DIR --outdir $SCRIPT_DIR --logDebug --singularity --batchSystem slurm --batchLogsDir ./logs --jobStore ./job_store --clean always
