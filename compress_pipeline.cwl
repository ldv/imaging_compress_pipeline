#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow
label: compress_pipeline

requirements:
- class: StepInputExpressionRequirement
- class: InlineJavascriptRequirement
- class: MultipleInputFeatureRequirement

inputs:
- id: flag_autocorrelation
  type: boolean?
- id: is_long_baseline
  type: boolean?
- id: msin
  type: Directory

outputs:
- id: inspect
  type: File
  outputSource:
  - inspect_step/inspect_file
- id: logfile
  type: File[]?
  outputSource:
  - dppp/logfile
- id: compressed
  type: File?
  outputSource:
  - compress/compressed
- id: ingest
  type: Any?
  outputSource:
  - format_ingest/ingest
- id: applied_fixes
  type: string[]
  outputSource:
  - identify_issues/issue_list
- id: unfixable_issues
  type: string[]
  outputSource:
  - identify_issues/unfixable_issues
- id: keep_input
  type: boolean
  outputSource: identify_issues/skip_compress
- id: input_storage_manager
  type: string
  outputSource: identify_issues/input_storage_manager

steps:
- id: identify_issues
  in:
  - id: msin
    source: msin
  - id: is_long_baseline
    source: is_long_baseline
  run: steps/identify_issues.cwl
  out:
  - issue_list
  - is_raw
  - unfixable_issues
  - msout
  - skip_compress
  - input_storage_manager
- id: fix_ai_2013
  in:
  - id: msin
    source: identify_issues/msout
  - id: apply
    valueFrom: $(self.includes("FIX_ANTENNA_TABLE"))
    source: identify_issues/issue_list
  run: steps/fix_antenna_information_2013.cwl
  out:
  - id: msout
- id: fix_weight_issue
  in:
  - id: msin
    source:
    - fix_ai_2013/msout
  - id: apply
    valueFrom: $(self.includes("FIX_WEIGHT_SPECTRUM"))
    source: identify_issues/issue_list
  run: steps/fix_weightissue_flagging.cwl
  out:
  - id: msout
- id: fix_ai_2015
  in:
  - id: msin
    source:
    - fix_weight_issue/msout
  - id: apply
    valueFrom: $(self.includes("FIX_BROKEN_TILES"))
    source: identify_issues/issue_list
  run: steps/fix_antenna_information_2015.cwl
  out:
  - id: msout
- id: fix_baselines
  in:
  - id: msin
    source:
    - fix_ai_2015/msout
  - id: apply
    valueFrom: $(self.includes("FIX_STATION_ADDER"))
    source: identify_issues/issue_list
  run: steps/fix_long_baselines.cwl
  out:
  - id: msout
- id: extract_sip_meta
  in:
  - id: msin
    source: 
    - dppp/msout
    - msin
    pickValue: first_non_null
  - id: compressed_file
    source: compress/compressed
  - id: skip
    source: identify_issues/skip_compress
  when: $(inputs.skip == false)
  run: steps/extract_sip_meta.cwl
  out:
  - id: ingest
- id: dppp
  in:
  - id: parset
    source: define_parset/output
  - id: msin
    source:
    - fix_baselines/msout
  - id: msout_name
    valueFrom: $("COMPRESSED_" + self.basename)
    source: fix_baselines/msout
  - id: writefullresflag
    default: true
  - id: storagemanager
    default: Dysco
  - id: databitrate
    default: 10
  - id: input_storage_manager
    source: identify_issues/input_storage_manager
  when: $(inputs.input_storage_manager != "DyscoStorageManager")
  run: steps/DPPP.cwl
  out:
  - id: msout
  - id: logfile
- id: format_ingest
  in:
  - id: metadata
    source: extract_sip_meta/ingest
  - id: output_name
    default: compressed
  - id: file_name
    valueFrom: '$(self ? self.basename : "skipped_so_no_file")'
    source: compress/compressed
  - id: skip
    source: identify_issues/skip_compress
  when: $(inputs.skip == false)
  run: steps/format_ingest.cwl
  out:
  - id: ingest
- id: define_parset
  label: define_parset
  in:
  - id: flag_autocorrelation
    source: flag_autocorrelation
  - id: is_raw
    source: identify_issues/is_raw
  run: steps/define_parset.cwl
  out:
  - id: output
- id: compress
  label: compress
  in:
  - id: directory
    source: 
    - dppp/msout
    - msin
    pickValue: first_non_null
  - id: skip
    source: identify_issues/skip_compress
  when: $(inputs.skip == false)
  run: steps/compress.cwl
  out:
  - id: compressed
- id: inspect_step
  in:
  - id: msin
    source: 
    - dppp/msout
    - msin
    pickValue: first_non_null
  - id: inspect_file_name
    source: 
    - dppp/msout
    - msin
    pickValue: first_non_null
    valueFrom: $(self.basename + "_inspect.h5")
  run: steps/create_inspect_dataset.cwl
  out:
  - inspect_file
